#ifndef _STRUCTURES_
#define _STRUCTURES_

typedef enum {Char, Int, Function}data_type;
typedef enum {Declaration,FunctionDeclaration,FunctionDefinition,Statement}action_type;
typedef enum {ArrayDeclarator,Declarator}declarator_type;
typedef enum {CompoundStat,IfElse,While,Return,Expression} stat_type;
typedef enum {Or,And,Eq,Ne,Lt,Gt,Le,Ge,Add,Sub,Mul,Div,Mod,Not,Minus,Plus,Addr,Deref,Store,Call,Print,Atoi,Itoa,Id,ChrLit,StrLit,IntLit} exp_type;

typedef struct _pointer{
	struct _pointer *next;
} is_pointer;

typedef struct _parameter{
	data_type d_type;
	is_pointer *pointer;
	char *id;
	struct _parameter *next;
}is_parameter;

typedef struct _declarator{
	is_pointer* pointer;
	char *id;
	declarator_type d_type;
	int array_size;
	struct _declarator* next;
} is_declarator;

typedef struct _variable_declaration{
	data_type d_type;
	is_declarator* declarator;
}is_variable_declaration;

typedef struct _expression{
	exp_type e_type;
	exp_type mother_type;
	union{
		char chr;
		char *str;
		int num;
		struct _expression *exp;
	} value;
	struct _expression *next;
} is_expression;

typedef struct _if_else {
	is_expression *exp;
	struct _statement *stat1;
	struct _statement *stat2;
} is_if_else;

typedef struct _while {
	is_expression *exp;
	struct _statement *stat;
} is_while;

typedef struct _statement{
	stat_type s_type;
	union{
		is_expression *exp;
		is_if_else *if_else_stat;
		is_while *while_stat;
		struct _function_body *compound_stat;
	} stat;
} is_statement;

typedef struct _function_body {
	action_type d_type;
	union{
		is_variable_declaration *var_dec;
		is_statement *stat;
	} f_body;
	struct _function_body *next;
} is_function_body;

typedef struct _function_declarator{
	is_pointer *pointer;
	char *id;
	is_parameter *param;
		
}is_function_declarator;

typedef struct _function_definition{
	data_type d_type;
	is_function_declarator *declarator;
	is_function_body *body;
}is_function_definition;

typedef struct _function_declaration{
	data_type d_type;
	is_function_declarator *declarator;
		
}is_function_declaration;

typedef struct _root{
	action_type d_type;	
	
	union{
		is_function_declaration *fun_dec;
		is_function_definition *fun_def;
		is_variable_declaration *var_dec;
	} dec;

	struct _root *next;
}is_root;

#endif

