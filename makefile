CC=gcc
CFLAGS=-ly -g
CNAME=qccompiler

YACC=yacc
YFILE=qccompiler.y
YFLAGS=-d -v -r all

LEX=lex
LFILE=qccompiler.l
LFLAGS=

ZIP=zip
ZNAME=qccompiler.zip

all: y.tab.c lex.yy.c
	$(CC) *.c -o $(CNAME) $(CFLAGS) 

y.tab.c: $(YFILE) 
	$(YACC) $(YFLAGS) $(YFILE)
	
lex.yy.c: $(LFILE)
	$(LEX) $(LFLAGS) $(LFILE)

clean:
	rm -f y.tab.c lex.yy.c y.output *.zip

zip:
	$(ZIP) $(ZNAME) *.l *.h *.c *.y
