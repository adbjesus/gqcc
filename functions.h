#include "structures.h"

is_root* insert_root_func_definition(is_function_definition*, is_root*);
is_root* insert_root_func_declaration(is_function_declaration*, is_root*);
is_root* insert_root_var_declaration(is_variable_declaration*, is_root*);
is_function_definition* insert_func_definition(data_type,is_function_declarator*,is_function_body*,is_function_body*);
is_function_body* insert_func_body_declaration(is_function_body*,is_variable_declaration*);
is_function_declaration* insert_func_declaration(data_type,is_function_declarator*);
is_function_declarator* insert_func_declarator(is_pointer*, char*, is_parameter*);
is_variable_declaration* insert_var_declaration(data_type,is_declarator*);
is_declarator* insert_declarator(is_declarator*,is_pointer*,char*,int*);
is_parameter* insert_param(data_type,is_pointer*,char*,is_parameter*);
is_pointer* insert_pointer(is_pointer*);
is_expression* insert_expression(exp_type,is_expression*,is_expression*);
is_expression* insert_call_expression(exp_type,char *,is_expression*);
is_expression* insert_call_expression2(is_expression*,is_expression*);
is_expression* insert_terminal(exp_type,union{char chr;int num;char* str;};);

is_statement* insert_expression_statement(is_expression*);
is_statement* insert_compound_statement(is_function_body*);
is_function_body* add_compound_statement(is_function_body*,is_statement*);
is_statement* insert_if_else(is_expression*,is_statement*,is_statement*);
is_statement* insert_while(is_expression*,is_statement*);
is_statement* insert_return(is_expression*);

