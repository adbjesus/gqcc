#include<stdio.h>
#include "symbol_table.h"
#include "structures.h"
#include "show.h"

void print_vars(var_list *vl){
	int i;
	var_list *var_aux;
	for(var_aux=vl;var_aux!=NULL;var_aux=var_aux->next){
		printf("%s\t",var_aux->id);
		switch(var_aux->type){
			case Int: printf("int");break;
			case Char:	printf("char");break;
			case Function: printf("function");break;
		}
		for(i=0;i<var_aux->npointer;i++) printf("*");
		if(var_aux->size!=-1) printf("[%d]",var_aux->size);
		if(var_aux->param==1) printf("\tparam");
		printf("\n");
	}
}

void print_table(prog_list *pl){
	int i;
	var_list *var_aux;
	func_list *fl;
	printf("===== Global Symbol Table =====\n");
	print_vars(pl->global_vars);
	for(fl=pl->funcs;fl!=NULL;fl=fl->next){
		printf("===== Function %s Symbol Table =====\n",fl->id);
		/*return*/
		printf("return\t");
		switch(fl->type){
			case Int: printf("int");break;
			case Char:	printf("char");break;
		}
		for(i=0;i<fl->npointer;i++) printf("*");
		printf("\n");
		/*parameters*/
		print_vars(fl->local_vars);
	}
}


int ind;

void indent(){
  int i;
  for(i=0;i<ind;i++)
    printf("  ");
}

void print_root(is_root *program){
  indent();printf("Program\n");
  while(1){
    if(program==NULL) break;
    ind++;
    if(program->d_type==FunctionDefinition)
      print_function_definition(program->dec.fun_def);
    else if(program->d_type==FunctionDeclaration) 
      print_function_declaration(program->dec.fun_dec);
    else if(program->d_type==Declaration) 
      print_variable_declaration(program->dec.var_dec);
    ind--;
    program = program->next;
  }
}

void print_data_type(data_type d){
  if(d==Char){
    indent();printf("Char\n");
  }
  else if(d==Int){
    indent();printf("Int\n");
  }
  return;
}

void print_function_declarator(is_function_declarator *declarator){
  indent();printf("FuncDeclarator\n");
  ind++;
  print_pointer(declarator->pointer);
  print_id(declarator->id);
  print_parameter(declarator->param);
  ind--;
  return;
}

void print_function_declaration(is_function_declaration *declaration){
  indent();printf("FuncDeclaration\n");
  ind++;
  print_data_type(declaration->d_type);
  print_function_declarator(declaration->declarator);
  ind--;
}


void print_pointer(is_pointer *p){
  if(p!=NULL){
    indent();printf("Pointer\n");
    print_pointer(p->next);
  }
  return;
}

void print_id(char* id){
  indent();printf("Id(%s)\n",id);
  return;
}

void print_parameter(is_parameter *p){
  if(p!=NULL){
    indent();printf("ParamDeclaration\n");
    ind++;
    print_data_type(p->d_type);
    print_pointer(p->pointer);
    print_id(p->id);
    ind--;
    print_parameter(p->next);
  }
  return;
}

void print_array_size(int s){
  indent();printf("IntLit(%d)\n",s);
}

void print_variable_declarator(is_declarator *declarator){
  if(declarator!=NULL){
    if(declarator->d_type==Declarator){
      indent();printf("Declarator\n");
      ind++;
      print_pointer(declarator->pointer);
      print_id(declarator->id);
    }
    else if(declarator->d_type==ArrayDeclarator){
      indent();printf("ArrayDeclarator\n");
      ind++;
      print_pointer(declarator->pointer);
      print_id(declarator->id);
      print_array_size(declarator->array_size);
    }
    ind--;
    print_variable_declarator(declarator->next);
  }
  return;
}


void print_variable_declaration(is_variable_declaration *declaration){
  indent();printf("Declaration\n");
  ind++;
  print_data_type(declaration->d_type);
  print_variable_declarator(declaration->declarator);
  ind--;
}

void print_expression_type(is_expression *exp){
  exp_type e = exp->e_type;
  if(e==Or) printf("Or\n");
  else if(e==And) printf("And\n");
  else if(e==Eq) printf("Eq\n");
  else if(e==Ne) printf("Ne\n");
  else if(e==Lt) printf("Lt\n");
  else if(e==Gt) printf("Gt\n");
  else if(e==Le) printf("Le\n");
  else if(e==Ge) printf("Ge\n");
  else if(e==Add) printf("Add\n");
  else if(e==Sub) printf("Sub\n");
  else if(e==Mul) printf("Mul\n");
  else if(e==Div) printf("Div\n");
  else if(e==Mod) printf("Mod\n");
  else if(e==Not) printf("Not\n");
  else if(e==Minus) printf("Minus\n");
  else if(e==Plus) printf("Plus\n");
  else if(e==Addr) printf("Addr\n");
  else if(e==Deref) printf("Deref\n");
  else if(e==Store) printf("Store\n");
  else if(e==Call) printf("Call\n");
  else if(e==Print) printf("Print\n");
  else if(e==Atoi) printf("Atoi\n");
  else if(e==Itoa) printf("Itoa\n");
  else if(e==Id) printf("Id(%s)\n",exp->value.str);
  else if(e==ChrLit) printf("ChrLit(%s)\n",exp->value.str);
  else if(e==StrLit) printf("StrLit(%s)\n",exp->value.str);
  else if(e==IntLit) printf("IntLit(%d)\n",exp->value.num);
  return;
}

void print_expression(is_expression *exp){
  exp_type e;
  
  while(exp!=NULL){
    indent();
    print_expression_type(exp);
    e = exp->e_type;
    if(e!=Id && e!=StrLit && e!=ChrLit && e!=IntLit){
      ind++;
      print_expression(exp->value.exp);
      ind--;
    }
    exp=exp->next;
  }
  
  return;
}

int count_compound_stat(is_function_body *cstat){
  int count;
  for(count=0;count<=1 && cstat!=NULL;count++,cstat=cstat->next);
  return count;
}

void print_compound_stat(is_function_body *cstat){
  int count=count_compound_stat(cstat);
  if(count>1){
    indent();
    printf("CompoundStat\n");
    ind++;
  }
  for(;cstat!=NULL;cstat=cstat->next) print_statement(cstat->f_body.stat);
  if(count>1)ind--;
  return;
}

void print_return_stat(is_expression *exp){
  indent();
  printf("Return\n");
  ind++;
  print_expression(exp);
  ind--;
  return;
}
void print_while_stat(is_while *stat){
  indent();
  printf("While\n");
  ind++;
  print_expression(stat->exp);
  if(stat->stat==NULL){indent();printf("Null\n");}
  print_statement(stat->stat);
  ind--;
  return;
}
void print_if_else_stat(is_if_else *stat){
  indent();
  printf("IfElse\n");
  ind++;
  print_expression(stat->exp);
  if(stat->stat1==NULL) {indent();printf("Null\n");}
  else print_statement(stat->stat1);
  if(stat->stat2==NULL) {indent();printf("Null\n");}
  else print_statement(stat->stat2);
  ind--;
  return;
}

void print_statement(is_statement *stat){
  if(stat==NULL) return;
  if(stat->s_type==Expression) print_expression(stat->stat.exp);
  else if(stat->s_type==CompoundStat) print_compound_stat(stat->stat.compound_stat);
  else if(stat->s_type==Return) print_return_stat(stat->stat.exp);
  else if(stat->s_type==IfElse) print_if_else_stat(stat->stat.if_else_stat);
  else if(stat->s_type==While) print_while_stat(stat->stat.while_stat);
}

void print_function_body(is_function_body *body){
  indent();printf("FuncBody\n");
  while(body!=NULL){
    ind++;
    if(body->d_type==Declaration)
      print_variable_declaration(body->f_body.var_dec);
    else if(body->d_type==Statement)
      print_statement(body->f_body.stat);
    ind--;
    body=body->next;
  }
}

void print_function_definition(is_function_definition *definition){
  indent();printf("FuncDefinition\n");
  ind++;
  print_data_type(definition->d_type);
  print_function_declarator(definition->declarator);
  print_function_body(definition->body);
  ind--;
}

