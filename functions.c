#include "functions.h"
#include "structures.h"
#include <stdlib.h>
#include <string.h>


is_root* insert_root_func_definition(is_function_definition* ife, is_root* r){
	is_root* root = (is_root*)malloc(sizeof(is_root));
	root->d_type = FunctionDefinition;
	root->dec.fun_def = ife;
	if(r==NULL) return root;

	is_root* aux;
	for(aux=r; aux->next!=NULL;aux=aux->next);
	aux->next=root;
	return r;
}

is_root* insert_root_func_declaration(is_function_declaration* ifd, is_root* r){
	is_root* root = (is_root*)malloc(sizeof(is_root));
	root->d_type = FunctionDeclaration;
	root->dec.fun_dec = ifd;
	if(r==NULL) return root;

	is_root* aux;
	for(aux=r; aux->next!=NULL;aux=aux->next);
	aux->next=root;
	return r;
}

is_root* insert_root_var_declaration(is_variable_declaration* ivd, is_root* r){
	is_root* root = (is_root*)malloc(sizeof(is_root));
	root->d_type = Declaration;
	root->dec.var_dec = ivd;
	if(r==NULL) return root;

	is_root* aux;
	for(aux=r; aux->next!=NULL;aux=aux->next);
	aux->next=root;
	return r;
}

is_function_definition* insert_func_definition(data_type d_type,is_function_declarator* fun_dec,is_function_body* var_dec,is_function_body* stat){
	is_function_definition* func = (is_function_definition*)malloc(sizeof(is_function_definition));
	func->d_type=d_type;
	func->declarator=fun_dec;
	func->body=var_dec;
	if(func->body==NULL) func->body=stat;
	else{
		is_function_body* aux;
		for(aux=func->body;aux->next!=NULL;aux=aux->next);
		aux->next=stat;
	}	
	return func;
}

is_function_body* insert_func_body_declaration(is_function_body* f_body,is_variable_declaration* var_dec){
	is_function_body* func = (is_function_body*)malloc(sizeof(is_function_body));
	func->d_type=Declaration;
	func->f_body.var_dec=var_dec;
	if(f_body==NULL) return func;

	is_function_body* aux;
	for(aux=f_body;aux->next!=NULL;aux=aux->next);
	aux->next=func;
	return f_body;
}

is_function_declaration* insert_func_declaration(data_type d_type,is_function_declarator* fun_dec){
	is_function_declaration* func = (is_function_declaration*)malloc(sizeof(is_function_declaration));
	func->d_type=d_type;
	func->declarator=fun_dec;
	return func;
}

is_function_declarator* insert_func_declarator(is_pointer* p, char* id,is_parameter* param){
	is_function_declarator* fun_dec = (is_function_declarator*)malloc(sizeof(is_function_declarator));
	fun_dec->pointer=p;
	fun_dec->id=strdup(id);
	fun_dec->param=param;
	return fun_dec;
}
	
is_variable_declaration* insert_var_declaration(data_type d_type,is_declarator* dec){
	is_variable_declaration* var = (is_variable_declaration*)malloc(sizeof(is_variable_declaration));
	var->declarator = dec;
	var->d_type=d_type;
	return var;
}

is_declarator* insert_declarator(is_declarator* dec,is_pointer* p,char* id,int* size){
	is_declarator* declarator = (is_declarator*)malloc(sizeof(is_declarator));
	declarator->pointer=p;
	declarator->id=strdup(id);
	if(size!=NULL){
		declarator->d_type=ArrayDeclarator;
		declarator->array_size=*size;
	}
	else{
		declarator->d_type=Declarator;
	}

	if(dec==NULL) return declarator;
	is_declarator* aux;
	for(aux=dec;aux->next!=NULL;aux=aux->next);
	aux->next=declarator;
	return dec;
}

is_parameter* insert_param(data_type d_type,is_pointer* p,char* id,is_parameter* param){
	is_parameter* parameter = (is_parameter*)malloc(sizeof(is_parameter));
	parameter->d_type=d_type;
	parameter->pointer=p;
	parameter->id=strdup(id);
	if(param==NULL) return parameter;
	is_parameter* aux;
	for(aux=param;aux->next!=NULL;aux=aux->next);
	aux->next=parameter;
	return param;
}

is_pointer* insert_pointer(is_pointer* p){
	is_pointer* point = (is_pointer*)malloc(sizeof(is_pointer));

	if(p==NULL) return point;
	is_pointer* aux;
	for(aux=p;aux->next!=NULL;aux=aux->next);
	aux->next=point;
	return p;
}

is_expression* insert_expression(exp_type e_type,is_expression* exp1,is_expression* exp2){
	is_expression* _exp = (is_expression*)malloc(sizeof(is_expression));
	_exp->e_type=e_type;
	if(e_type==Deref && exp2!=NULL){
		exp1=insert_expression(Add,exp1,exp2);
		exp2=NULL;
	}
	_exp->value.exp=exp1;
	if(exp2!=NULL){
		exp1->next=exp2;
	}
	else{
		exp1->next=NULL;	
	} 
	return _exp;
}

is_expression* insert_call_expression(exp_type e_type,char *id,is_expression* exp1){
	is_expression* _exp = (is_expression*)malloc(sizeof(is_expression));
	is_expression* _exp2 = (is_expression*)malloc(sizeof(is_expression));
	
	_exp->e_type=e_type;
	
	_exp2->e_type=Id;
	_exp2->value.str=strdup(id);
	
	_exp->value.exp=_exp2;
	
	if(exp1!=NULL){
		_exp2->next=exp1;
	}
	else{
		_exp2->next=NULL;
	}
	return _exp;
}

is_expression* insert_call_expression2(is_expression *exp1,is_expression* exp2){
	if(exp1==NULL) return exp2;
	is_expression *aux;
	for(aux=exp1;aux->next!=NULL;aux=aux->next);
	aux->next=exp2;
	return exp1;
}

is_expression* insert_terminal(exp_type e_type,union{char chr;int num;char* str;}term){
	is_expression* _exp = (is_expression*)malloc(sizeof(is_expression));
	_exp->e_type=e_type;
	if(e_type==Id || e_type==StrLit) _exp->value.str=strdup(term.str);
	else if(e_type==ChrLit) _exp->value.str=strdup(term.str);
	else if(e_type==IntLit) _exp->value.num=term.num;
	_exp->next=NULL;
	return _exp;
}

is_statement* insert_expression_statement(is_expression* exp1){
	is_statement* stat = (is_statement *)malloc(sizeof(is_statement));
	stat->s_type=Expression;
	stat->stat.exp=exp1;
	return stat;
}

is_statement* insert_compound_statement(is_function_body* cstat){
	if(cstat==NULL) return NULL;
	is_statement* stat = (is_statement *)malloc(sizeof(is_statement));
	stat->s_type=CompoundStat;
	stat->stat.compound_stat=cstat;
	return stat;
}

is_function_body* add_compound_statement(is_function_body* cst,is_statement* stat){
	is_function_body* cstat;
	if(stat==NULL && cst==NULL) return NULL;
	if(stat==NULL) cstat = NULL;
	else{
		cstat = (is_function_body*)malloc(sizeof(is_function_body));
		cstat->f_body.stat=stat;
		cstat->d_type=Statement;
	}
	if(cst==NULL) return cstat;
	is_function_body* aux;
	for(aux=cst;aux->next!=NULL;aux=aux->next);
	aux->next=cstat;
	return cst;
}

is_statement* insert_if_else(is_expression* exp,is_statement* stat1,is_statement* stat2){
	is_statement* stat = (is_statement *)malloc(sizeof(is_statement));
	is_if_else* ie = (is_if_else *)malloc(sizeof(is_if_else));
	ie->exp=exp;
	ie->stat1=stat1;
	ie->stat2=stat2;
	stat->s_type=IfElse;
	stat->stat.if_else_stat = ie;
	return stat;
}
is_statement* insert_while(is_expression* exp,is_statement* stat1){
	is_statement* stat = (is_statement *)malloc(sizeof(is_statement));
	is_while* w = (is_while *)malloc(sizeof(is_while));
	w->exp=exp;
	w->stat=stat1;
	stat->s_type=While;
	stat->stat.while_stat = w;
	return stat;	
}

is_statement* insert_return(is_expression* exp1){
	is_statement* stat = (is_statement *)malloc(sizeof(is_statement));
	stat->s_type=Return;
	stat->stat.exp=exp1;
	return stat;
}
