#include "structures.h"
#include "symbol_table.h"

void print_table(prog_list*);
void indent();
void print_root(is_root *program);
void print_function_declaration(is_function_declaration *declaration);
void print_data_type(data_type d);
void print_stat_type(stat_type);
void print_function_declarator(is_function_declarator *declarator);
void print_pointer(is_pointer *p);
void print_id(char* id);
void print_parameter(is_parameter *p);
void print_array_size(int s);
void print_variable_declarator(is_declarator *declarator);
void print_variable_declaration(is_variable_declaration *declaration);
void print_function_definition(is_function_definition *definition);
void print_function_body(is_function_body *b);
void print_expression(is_expression*);
void print_compound_stat(is_function_body*);
void print_return_stat(is_expression*);
void print_while_stat(is_while*);
void print_if_else_stat(is_if_else*);
void print_expression_type(is_expression*);
void print_statement(is_statement*);

