%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structures.h"
#include "functions.h"
#include "show.h"
#include "symbol_table.h"
#include "semantic.h"

is_root* program;
prog_list* symbols;
%}

%union{
  is_parameter* param;
  is_function_definition* fun_def;
  is_function_declaration* fun_declaration;
  is_variable_declaration* var_dec;
  is_declarator* var_declarator;
  is_function_declarator* fun_declarator;
  is_function_body* fun_body;
  is_root* root;
  is_pointer* pointer;
  is_expression* expre;
  is_statement* stat;
  data_type d_type;
  int num;
  char* str;
  char chr;
}

%token ATOI
%token ITOA
%token CHAR
%token INT
%token AND
%token AMP
%token AST
%token COMMA
%token DIV
%token MINUS
%token MOD
%token OR
%token PLUS
%token SEMI
%token EQ
%token GE
%token GT
%token LE
%token LT
%token NE
%token ASSIGN
%token NOT
%token LPAR
%token RPAR
%token LSQ
%token RSQ
%token LBRACE
%token RBRACE
%token IF 
%token ELSE
%token WHILE
%token PRINTF
%token RETURN
%token RESERVED
%token COMMENT
%token<str> STRLIT
%token<str> CHRLIT
%token<num> INTLIT
%token<str> ID

%type<root>start
%type<param>parameter_declaration
%type<fun_def>function_definition
%type<fun_declaration>function_declaration
%type<fun_declarator>function_declarator
%type<fun_body>declaration_0_n
%type<var_dec>declaration
%type<var_declarator>declarator
%type<d_type>type_specifier
%type<pointer>asterisk_0_n
%type<expre>expression
%type<expre>comma_expression_1_n
%type<stat>statement
%type<fun_body>statement_0_n


%left COMMA
%right ASSIGN
%left OR
%left AND
%left NE EQ
%left GE GT LE LT
%left PLUS MINUS
%left AST DIV MOD
%right NOT AMP
%right LPAR LSQ LBRACE
%left RPAR RSQ RBRACE
%nonassoc IF ELSE 

%%
start: start function_definition {$$=insert_root_func_definition($2,$1);}
     | start function_declaration {$$=insert_root_func_declaration($2,$1);}
     | start declaration {$$=insert_root_var_declaration($2,$1);}
     | function_definition {$$=insert_root_func_definition($1,NULL);program=$$;}
     | function_declaration {$$=insert_root_func_declaration($1,NULL);program=$$;}
     | declaration {$$=insert_root_var_declaration($1,NULL);program=$$;}
     ;

function_definition: type_specifier function_declarator LBRACE declaration_0_n statement_0_n RBRACE {$$=insert_func_definition($1,$2,$4,$5);}
                   ;

declaration_0_n: declaration_0_n declaration {$$=insert_func_body_declaration($1,$2);}
               | /*empty*/ {$$=NULL;}
               ;

function_declaration: type_specifier function_declarator SEMI {$$=insert_func_declaration($1,$2);}
                    ;

function_declarator: asterisk_0_n ID LPAR RPAR {$$=insert_func_declarator($1,$2,NULL);}
                   | asterisk_0_n ID LPAR parameter_declaration RPAR {$$=insert_func_declarator($1,$2,$4);}
                   ;

parameter_declaration: parameter_declaration COMMA type_specifier asterisk_0_n ID {$$=insert_param($3,$4,$5,$1);}
                     | type_specifier asterisk_0_n ID {$$=insert_param($1,$2,$3,NULL);}
                     ;

declaration: type_specifier declarator SEMI {$$=insert_var_declaration($1,$2);}
           ;

type_specifier: CHAR {$$=Char;}
              | INT {$$=Int;}
              ;

declarator: declarator COMMA asterisk_0_n ID {$$=insert_declarator($1,$3,$4,NULL);}
          | declarator COMMA asterisk_0_n ID LSQ INTLIT RSQ {$$=insert_declarator($1,$3,$4,&$6);}
          | asterisk_0_n ID {$$=insert_declarator(NULL,$1,$2,NULL);}
          | asterisk_0_n ID LSQ INTLIT RSQ {$$=insert_declarator(NULL,$1,$2,&$4);}
          ;

asterisk_0_n: asterisk_0_n AST {$$=insert_pointer($1);}
            | {$$=NULL;}
            ;

statement: SEMI {$$=NULL;}
         | expression SEMI {$$=insert_expression_statement($1);}

         | LBRACE statement_0_n RBRACE {$$=insert_compound_statement($2);}

         | IF LPAR expression RPAR statement {$$=insert_if_else($3,$5,NULL);}
         | IF LPAR expression RPAR statement ELSE statement {$$=insert_if_else($3,$5,$7);}

         | WHILE LPAR expression RPAR statement {$$=insert_while($3,$5);}

         | RETURN expression SEMI {$$=insert_return($2);}
         ;

statement_0_n: statement_0_n statement {$$=add_compound_statement($1,$2);}
             | /*empty*/ {$$=NULL;}
             ;

expression: expression ASSIGN expression {$$=insert_expression(Store,$1,$3);}

          | expression AND expression {$$=insert_expression(And,$1,$3);}
          | expression OR expression {$$=insert_expression(Or,$1,$3);}

          | expression EQ expression {$$=insert_expression(Eq,$1,$3);}
          | expression NE expression {$$=insert_expression(Ne,$1,$3);}
          | expression LT expression {$$=insert_expression(Lt,$1,$3);}
          | expression GT expression {$$=insert_expression(Gt,$1,$3);}
          | expression LE expression {$$=insert_expression(Le,$1,$3);}
          | expression GE expression {$$=insert_expression(Ge,$1,$3);}

          | expression PLUS  expression {$$=insert_expression(Add,$1,$3);}
          | expression MINUS expression {$$=insert_expression(Sub,$1,$3);}
          | expression AST   expression {$$=insert_expression(Mul,$1,$3);}
          | expression DIV   expression {$$=insert_expression(Div,$1,$3);}
          | expression MOD   expression {$$=insert_expression(Mod,$1,$3);}

          | AMP   expression {$$=insert_expression(Addr,$2,NULL);}
          | AST   expression {$$=insert_expression(Deref,$2,NULL);}
          | PLUS  expression {$$=insert_expression(Plus,$2,NULL);}
          | MINUS expression {$$=insert_expression(Minus,$2,NULL);}
          | NOT   expression {$$=insert_expression(Not,$2,NULL);}

          | expression LSQ expression RSQ {$$=insert_expression(Deref,$1,$3);}

          | ID LPAR RPAR {$$=insert_call_expression(Call,$1,NULL);}
          | ID LPAR comma_expression_1_n RPAR {$$=insert_call_expression(Call,$1,$3);}
          
					| PRINTF LPAR expression RPAR {$$=insert_expression(Print,$3,NULL);}
          | ATOI LPAR expression RPAR {$$=insert_expression(Atoi,$3,NULL);}

          | ITOA LPAR expression COMMA expression RPAR {$$=insert_expression(Itoa,$3,$5);}

          | ID {$$=insert_terminal(Id,$1);}
          | INTLIT {$$=insert_terminal(IntLit,$1);}
          | CHRLIT {$$=insert_terminal(ChrLit,$1);}
          | STRLIT {$$=insert_terminal(StrLit,$1);}
          | LPAR expression RPAR {$$=$2;}
          ;

comma_expression_1_n: comma_expression_1_n COMMA expression {$$=insert_call_expression2($1,$3);}
                    | expression 
                    ;


%%

int main(int argc, char *argv[]){
	int i,past=0,pst=0;

	/*Check variables*/
	for(i=1;i<argc;i++){
		if(strcmp(argv[i],"-s")==0){
			pst=1;
		}
		else if(strcmp(argv[i],"-t")==0){
			past=1;
		}
	}

  if(yyparse()!=0) return 0;
	if(past==1)	print_root(program);
	if(past==1 && pst==0) return 0;
	if((symbols=semantic_analysis(program))==NULL) return 0;
	if(pst==1) print_table(symbols);
  return 0;
}



