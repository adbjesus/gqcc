#include "structures.h"
#include "symbol_table.h"

int function_defined(prog_list *pl,char id[]);
prog_list *semantic_analysis(is_root* r);
var_list *semantic_vars(is_variable_declaration *var_dec, var_list *vars,int global);
void semantic_analysis_var_declaration(prog_list *pl,is_variable_declaration *var_dec);
void check_func_return(is_function_declarator *fun_dec, data_type otype, data_type ntype, int npointers);
void check_count_params(is_function_declarator *fun_dec,int count_params);
void check_func_params(is_function_declarator *fun_dec, var_list *vl);
void check_func(func_list *fl,is_function_declarator *fun_dec,data_type ntype,int def);
void semantic_analysis_fun_declaration(prog_list *pl,is_function_declarator *fun_dec,data_type d_type,int def);
void semantic_analysis_stat(prog_list *pl, func_list *fl, is_statement *stat);
void semantic_analysis_fun_body(prog_list *pl,func_list *fl,is_function_body *body);
void semantic_analysis_fun_definition(prog_list *pl,is_function_definition *fun_def);	
void append_symbol(var_list *vl, var_list *s);
var_list *create_symbol();
var_list *var_lookup(var_list *vl,char *id);
void append_env(func_list *fl, func_list *f);
func_list *create_env();
func_list *func_lookup(func_list *fl, char *id);

