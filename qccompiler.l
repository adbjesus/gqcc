keywords ("auto")|("break")|("case")|("const")|("continue")|("default")|("do")|("double")|("enum")|("extern")|("float")|("for")|("goto")|("long")|("register")|("short")|("signed")|("sizeof")|("static")|("struct")|("switch")|("typedef")|("union")|("unsigned")|("void")|("volatile")|("++")|("--")
id ([a-z]|[A-Z]|"_")([a-z]|[A-Z]|"_"|[0-9])*
strlit (\"([^\n\\\"]|(\\.))*\")
unterstr (\"(([^\n\\\"])|(\\.))*)((\\\n)|(\n)|(\\<EOF>)|(<EOF>))
chrlit ([^\n\\\']|(\\.))
unterchr (\'([^\n\\\']|(\\.))*)((\\\n)|(\n)|(\\<EOF>)|(<EOF>))
intlit (([1-9])([0-9])*)|(0)
%x COMM 

%{
	#include "structures.h"
	#include "y.tab.h"
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	int col=1;
	int line=1;
	int last[2];
%}

%%
"atoi" {col+=yyleng;return ATOI;}
"itoa" {col+=yyleng;return ITOA;}

"char" {col+=yyleng;return CHAR;}
"int" {col+=yyleng;return INT;}

"&&" {col+=yyleng;return AND;}
"&" {col+=yyleng;return AMP;}
"*" {col+=yyleng;return AST;}
"," {col+=yyleng;return COMMA;}
"/" {col+=yyleng;return DIV;}
"-" {col+=yyleng;return MINUS;}
"%" {col+=yyleng;return MOD;}
"||" {col+=yyleng;return OR;}
"+" {col+=yyleng;return PLUS;}
";" {col+=yyleng;return SEMI;}

"==" {col+=yyleng;return EQ;}
">=" {col+=yyleng;return GE;}
">" {col+=yyleng;return GT;}
"<=" {col+=yyleng;return LE;}
"<" {col+=yyleng;return LT;}
"!=" {col+=yyleng;return NE;}
"=" {col+=yyleng;return ASSIGN;}
"!" {col+=yyleng;return NOT;}

"(" {col+=yyleng;return LPAR;}
")" {col+=yyleng;return RPAR;}
"[" {col+=yyleng;return LSQ;}
"]" {col+=yyleng;return RSQ;}
"{" {col+=yyleng;return LBRACE;}
"}" {col+=yyleng;return RBRACE;}

"if" {col+=yyleng;return IF;} 
"else" {col+=yyleng;return ELSE;}
"while" {col+=yyleng;return WHILE;}

"printf" {col+=yyleng;return PRINTF;}
"return" {col+=yyleng;return RETURN;}

{keywords} {col+=yyleng;return RESERVED;}

"/*" {BEGIN COMM;last[0]=line;last[1]=col;col+=yyleng;}
<COMM>"*/" {col+=yyleng;BEGIN 0;}
<COMM>. {col++;}
<COMM>\n {line++;col=1;}
<COMM><<EOF>> {printf("Line %d, col %d: unterminated comment\n",last[0],last[1]);return 0;}

{strlit} {yylval.str=strdup(yytext);col+=yyleng;return STRLIT;}
{unterstr} {printf("Line %d, col %d: unterminated string constant\n",line,col);col=1;line++;}

\'{chrlit}{0,1}\' {yylval.str=strdup(yytext);col+=yyleng;return CHRLIT;}
\'{chrlit}({chrlit})+\' {printf("Line %d, col %d: multi-character char constant\n",line,col);col+=yyleng;}
{unterchr} {printf("Line %d, col %d: unterminated char constant\n",line,col);col=1;line++;}

{intlit} {yylval.num=atoi(yytext);col+=yyleng;return INTLIT;}
{id} {yylval.str=strdup(yytext);col+=yyleng;return ID;}
[^" "\n\t] {printf("Line %d, col %d: illegal character ('%c')\n",line,col,yytext[0]);col+=yyleng;}

<<EOF>> {return 0;}
. {col+=yyleng;}
\n {line++;col=1;}
%%
void yyerror (char *s) {
	printf ("Line %d, col %d: %s: %s\n",line,col-strlen(yytext),s,yytext);
}

int yywrap(){
  return 1;
}



