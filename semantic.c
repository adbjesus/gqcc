#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "semantic.h"
#include "structures.h"
#include "symbol_table.h"

int function_defined(prog_list *pl,char id[]){
	func_list *fl;
	fl = func_lookup(pl->funcs,id);
	if(fl==NULL) return -2;
	if(fl->def==0) return -1;
	
	return 0;
}

prog_list *semantic_analysis(is_root* r){
	is_root* aux;

	prog_list *pl = (prog_list*)malloc(sizeof(prog_list));

	for(aux=r;aux!=NULL;aux=aux->next){
		switch(aux->d_type){
			case Declaration: semantic_analysis_var_declaration(pl,aux->dec.var_dec);break;
			case FunctionDeclaration: semantic_analysis_fun_declaration(pl,aux->dec.fun_dec->declarator,aux->dec.fun_dec->d_type,0);break;
			case FunctionDefinition: semantic_analysis_fun_definition(pl,aux->dec.fun_def);break;
			default: break;
		}
	}
	if(function_defined(pl,"main")<0){
		printf("Function main is not defined\n");
		exit(0);
	}
	return pl;
}

var_list *semantic_vars(is_variable_declaration *var_dec, var_list *vars,int global){
	is_declarator *aux;
	var_list *vl,*cur,*tmp;
	is_pointer *p;
	vl = NULL;

	for(aux=var_dec->declarator;aux!=NULL;aux=aux->next){
		tmp = var_lookup(vars,aux->id);
		if(tmp!=NULL){
			if(global==1 && var_dec->d_type==tmp->type) continue;
			else{
				printf("Symbol %s redefined\n",aux->id);
			 	exit(0);
			}
		}
		if(vl==NULL) cur=vl=create_symbol();
		else cur=cur->next=create_symbol();

		cur->id=aux->id;
		cur->type=var_dec->d_type;
		if(aux->d_type==ArrayDeclarator){
			if(aux->array_size<1){
				printf("Size of array %s is not positive\n",cur->id);
				exit(0);
			}
			cur->size=aux->array_size;
		}
		for(p=aux->pointer;p!=NULL;p=p->next,cur->npointer++);
	}
	
	return vl;
}

void semantic_analysis_var_declaration(prog_list *pl,is_variable_declaration *var_dec){
	var_list *vl;
	vl=semantic_vars(var_dec,pl->global_vars,1);
	if(vl!=NULL){
		if(pl->global_vars==NULL) pl->global_vars=vl;
		else append_symbol(pl->global_vars,vl);
	}
}

void check_func_return(is_function_declarator *fun_dec, data_type otype, data_type ntype, int npointers){
	int i,count;
	is_pointer *p;
	char str[100],str2[100];
	/*count pointers*/
	for(count=0,p=fun_dec->pointer;p!=NULL;p=p->next,count++);
	/*check return type*/
	if(otype!=ntype || count!=npointers){
		switch(ntype){
			case Int: strcpy(str,"int");break;
			case Char: strcpy(str,"char");break;
		}
		switch(otype){
			case Int: strcpy(str2,"int");break;
			case Char: strcpy(str2,"char");break;
		}
		printf("Conflicting types in function %s declaration/definition (got %s",fun_dec->id,str);
		for(i=0;i<count;i++) printf("*");
		printf(", required %s",str2);
		for(i=0;i<npointers;i++) printf("*");
		printf(")\n");
		exit(0);
	}
}

void check_count_params(is_function_declarator *fun_dec,int count_params){
	is_parameter *par;
	is_pointer *p;	
	int count;
	/*Count params*/
	for(par=fun_dec->param,count=0;par!=NULL;par=par->next,count++);

	if(count!=count_params){
		printf("Conflicting numbers of arguments in function %s declaration/definition (got %d, required %d)\n",fun_dec->id,count,count_params);
		exit(0);
	}
}

void check_func_params(is_function_declarator *fun_dec, var_list *vl){
	int count,i;
	is_parameter *par;
	is_pointer *p;
	char str[100],str2[100];

	/*Check params types and names*/
	for(par=fun_dec->param;par!=NULL && vl!=NULL;par=par->next,vl=vl->next){
		if(strcmp(par->id,vl->id)!=0){
			printf("Conflicting argument names in function %s declaration/definition (got %s, required %s)\n",fun_dec->id,par->id,vl->id);
			exit(0);
		}
		for(count=0,p=par->pointer;p!=NULL;p=p->next,count++);
		if(par->d_type!=vl->type || count!=vl->npointer){
			switch(par->d_type){
				case Int: strcpy(str,"int");break;
				case Char: strcpy(str,"char");break;
			}
			switch(vl->type){
				case Int: strcpy(str2,"int");break;
				case Char: strcpy(str2,"char");break;
			}
			printf("Conflicting types in function %s declaration/definition (got %s",fun_dec->id,str);
			for(i=0;i<count;i++) printf("*");
			printf(", required %s",str2);
			for(i=0;i<vl->npointer;i++) printf("*");
			printf(")\n");
			exit(0);
		}
	}
	
}

void check_func(func_list *fl,is_function_declarator *fun_dec,data_type ntype,int def){
	func_list *aux;
	var_list *vl;
	int count;
	/*Check if it was redefined*/
	aux = func_lookup(fl,fun_dec->id);
	if(aux==NULL){
		printf("Symbol %s redefined\n",fun_dec->id);
		exit(0);
	}
	if(def==1 && aux->def==1){
		printf("Function %s redefined\n",fun_dec->id);
		exit(0);
	}
	if(def==1) aux->def=1;

	/*Check if return is the same*/
	check_func_return(fun_dec,aux->type,ntype,aux->npointer);

	/*Check if parameters are the same*/
	for(vl=aux->local_vars,count=0;vl!=NULL;vl=vl->next,count++)
		if(vl->param==0) break;
	check_count_params(fun_dec,count);
	check_func_params(fun_dec,aux->local_vars);

}

void semantic_analysis_fun_declaration(prog_list *pl,is_function_declarator *fun_dec,data_type d_type,int def){
	var_list *vl,*vl2;
	func_list *fl;
	is_pointer *p;
	is_parameter *par;
	int count1,count2,i;
	char str[100],str2[100];

	vl = var_lookup(pl->global_vars,fun_dec->id);
	if(vl!=NULL){
		check_func(pl->funcs,fun_dec,d_type,def);
	}
	else{
		vl=create_symbol();
		if(pl->global_vars==NULL)	pl->global_vars=vl;
		else append_symbol(pl->global_vars,vl);
	
		vl->id=fun_dec->id;
		vl->type=Function;

		fl=create_env();
		if(pl->funcs==NULL) pl->funcs=fl;
		else append_env(pl->funcs,fl);

		fl->id=fun_dec->id;
		fl->type=d_type;
		fl->def=def;
			
		for(p=fun_dec->pointer;p!=NULL;p=p->next,fl->npointer++);

		for(par=fun_dec->param;par!=NULL;par=par->next){
			vl=var_lookup(fl->local_vars,par->id);
			if(vl!=NULL){
				printf("Symbol %s redefined\n",vl->id);
				exit(0);
			}
			vl=create_symbol();
			if(fl->local_vars==NULL) fl->local_vars=vl;
			else append_symbol(fl->local_vars,vl);
			
			vl->id=par->id;
			vl->type=par->d_type;
			for(p=par->pointer;p!=NULL;p=p->next,vl->npointer++);
			vl->param=1;
		}
		if(strcmp(fl->id,"main")==0){
			check_func_return(fun_dec,Int,d_type,0);
			check_count_params(fun_dec,2);
			vl = create_symbol();
			vl->type = Int;
			vl->param = 1;
			vl->id=fl->local_vars->id;
			vl->next=create_symbol();
			vl->next->type = Char;
			vl->next->param = 1;
			vl->next->id = fl->local_vars->next->id;
			vl->next->npointer = 2;
			check_func_params(fun_dec,vl);
		}
	}
}

void semantic_analysis_exp(prog_list *pl, func_list *fl,is_expression *exp){
	
}

void semantic_analysis_stat(prog_list *pl, func_list *fl, is_statement *stat){
	if(stat==NULL) return;
	switch(stat->s_type){
		case CompoundStat: semantic_analysis_fun_body(pl,fl,stat->stat.compound_stat);break;
		case IfElse: 
			semantic_analysis_exp(pl,fl,stat->stat.if_else_stat->exp);
			semantic_analysis_stat(pl,fl,stat->stat.if_else_stat->stat1);
			semantic_analysis_stat(pl,fl,stat->stat.if_else_stat->stat2);
			break;
		case While: 
			semantic_analysis_exp(pl,fl,stat->stat.while_stat->exp);
			semantic_analysis_stat(pl,fl,stat->stat.while_stat->stat);
			break;
		case Return: 
		case Expression: 
			semantic_analysis_exp(pl,fl,stat->stat.exp);
			break;
	}	
}

void semantic_analysis_fun_body(prog_list *pl,func_list *fl,is_function_body *body){
	var_list *vl;

	for(;body!=NULL;body=body->next){
		if(body->d_type==Declaration){
			vl=semantic_vars(body->f_body.var_dec,fl->local_vars,0);
			if(vl!=NULL){
				if(fl->local_vars==NULL) fl->local_vars=vl;
				else append_symbol(fl->local_vars,vl);
			}
		}
		else if(body->d_type==Statement){
			semantic_analysis_stat(pl,fl,body->f_body.stat);
		}
	}

}

void semantic_analysis_fun_definition(prog_list *pl,is_function_definition *fun_def){	
	func_list *fl;

	semantic_analysis_fun_declaration(pl,fun_def->declarator,fun_def->d_type,1);
	fl=func_lookup(pl->funcs,fun_def->declarator->id);
	
	semantic_analysis_fun_body(pl,fl,fun_def->body);
}

void append_symbol(var_list *vl, var_list *s){
	var_list *aux;
	for(aux=vl;aux->next!=NULL;aux=aux->next);
	aux->next=s;
}

var_list *create_symbol(){
	var_list *aux;
	aux=(var_list *)malloc(sizeof(var_list));	
	aux->size=-1;
	aux->npointer=0;
	aux->param=0;
	aux->next=NULL;
	return aux;
}

var_list *var_lookup(var_list *vl,char *id){
	var_list *aux;
	for(aux=vl;aux!=NULL;aux=aux->next){
		if(strcmp(id,aux->id)==0) return aux;
	}
	return aux;
}

void append_env(func_list *fl, func_list *f){
	func_list *aux;
	for(aux=fl;aux->next!=NULL;aux=aux->next);
	aux->next=f;
}

func_list *create_env(){
	func_list *aux;
	aux=(func_list *)malloc(sizeof(func_list));	
	aux->npointer=0;
	aux->def=0;
	aux->next=NULL;
	return aux;
}

func_list *func_lookup(func_list *fl, char *id){
	func_list *aux;
	for(aux=fl;aux!=NULL;aux=aux->next){
		if(strcmp(id,aux->id)==0) return aux;
	}
	return aux;
}


