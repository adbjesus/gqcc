#include "structures.h"
#ifndef _SYMBOL_TABLE_
#define _SYMBOL_TABLE_

typedef struct _t1{
	data_type type;
	char* id;
	int npointer;
	int size;
	int param;
	struct _t1 *next;
} var_list;

typedef struct _t2{
	data_type type;
	char* id;
	int npointer;
	int def;
	var_list *local_vars;	
	struct _t2 *next;
} func_list;

typedef struct _t3{
	var_list *global_vars;
	func_list *funcs;
} prog_list;

#endif
